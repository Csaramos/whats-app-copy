
function loadXMLDoc (numberOfRecords) {
  var MsgRecived = [];
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      MsgRecived = JSON.parse(this.responseText).results;
      Messages(MsgRecived);
    }
  };
  xhttp.open("GET", "https://randomuser.me/api/?results=" + numberOfRecords, true);
  xhttp.send();
}

var Messages = function (MyMsgs) {

  var Contacts = document.getElementById("Contacts");


  for (var x = 0; x < MyMsgs.length; x++) {

    var contacts_info = document.createElement('div');    
    contacts_info.setAttribute('class', 'Contacts-Info');

    var ContactImg = document.createElement('img');

    ContactImg.setAttribute('class', 'Contacts-Content-Pic');
    ContactImg.setAttribute('src', MyMsgs[x].picture.large);

    contacts_info.appendChild(ContactImg);

    var divNameMsg = document.createElement('div');
    divNameMsg.setAttribute("class", "Contacts-Content-imgname");

    var contactName = document.createElement('div');
    contactName.setAttribute('class', "Contacts-Content-Name");
    contactName.appendChild(document.createTextNode(MyMsgs[x].name.first+" "+MyMsgs[x].name.last));

    var lastMsg = document.createElement('div');
    lastMsg.setAttribute('class', "Contacts-Content-Message");
    lastMsg.appendChild(document.createTextNode(MyMsgs[x].name.title+" "+MyMsgs[x].name.first+" "+MyMsgs[x].name.last+" "+MyMsgs[x].gender+" "+MyMsgs[x].location.street));

    divNameMsg.appendChild(contactName);
    divNameMsg.appendChild(lastMsg);

    contacts_info.appendChild(divNameMsg);


    Contacts.appendChild(contacts_info);
  } 
}

loadXMLDoc(50);
